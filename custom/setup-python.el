(use-package company-anaconda
  :config
  (eval-after-load "company"
    '(add-to-list 'company-backends 'company-anaconda))
  (add-hook 'python-mode-hook 'anaconda-mode))

(defadvice python-send-region (around advice-python-send-region-goto-end)
  "Fix a little bug if the point is not at the prompt when you do
    C-c C-[rc]"
  (let ((oldpoint (with-current-buffer (process-buffer (python-proc)) (point)))
    	(oldinput
    	 (with-current-buffer (process-buffer (python-proc))
    	   (goto-char (point-max))
    	   ;; Do C-c C-u, but without modifying the kill ring:
    	   (let ((pmark (process-mark (get-buffer-process (current-buffer)))))
    	     (when (> (point) (marker-position pmark))
    	       (let ((ret (buffer-substring pmark (point))))
                 (delete-region pmark (point))
                 ret))))))
    ad-do-it
    (with-current-buffer (process-buffer (python-proc))
      (when oldinput (insert oldinput))
      (goto-char oldpoint))))
(ad-enable-advice 'python-send-region 'around 'advice-python-send-region-goto-end)
(ad-activate 'python-send-region)

(use-package company-jedi
  :config
  (jedi:setup)
  (add-hook 'python-mode-hook
            (lambda ()
              (add-to-list 'company-backends 'company-jedi))))

(provide 'setup-python)
