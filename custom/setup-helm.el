(use-package helm
  :init
  (require 'helm-config)
  (require 'helm-grep)
  (if (version< "26.0.50" emacs-version) (eval-when-compile (require 'helm-lib)))

  ;; Hide minibuffer when helm is active
  ;; https://www.reddit.com/r/emacs/comments/3asbyn/new_and_very_useful_helm_feature_enter_search/
  (defun helm-hide-minibuffer-maybe ()
    (when (with-helm-buffer helm-echo-input-in-header-line)
      (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
        (overlay-put ov 'window (selected-window))
        (overlay-put ov 'face (let ((bg-color (face-background 'default nil)))
                                `(:background ,bg-color :foreground ,bg-color)))
        (setq-local cursor-type nil))))

  (add-hook 'helm-minibuffer-set-up-hook 'helm-hide-minibuffer-maybe)

  ;; Search Eshell history with helm
  (add-hook 'eshell-mode-hook
            (lambda ()
              (define-key eshell-mode-map
                (kbd "M-l")  'helm-eshell-history)))

  ;; Save current position to mark ring
  (add-hook 'helm-goto-line-before-hook 'helm-save-current-pos-to-mark-ring)

  ;; General config
  (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)

  (setq helm-scroll-amount 4 ;; Scroll amount when scrolling another window in a helm session
        helm-ff-search-library-in-sexp t ;; Search for library in `require' and `declare-function' sexp.
        helm-split-window-in-side-p t ;; Open helm buffer inside current window instead of occupying a whole other window
        helm-echo-input-in-header-line t ;; Send current input in header-line when non-nil
        helm-ff-file-name-history-use-recentf t
        helm-move-to-line-cycle-in-source t ;; Cycle to start or end of list when reach resp. end or start
        helm-buffer-skip-remote-checking t
        helm-mode-fuzzy-match t
        helm-buffers-fuzzy-matching t
        helm-semantic-fuzzy-match t
        helm-M-x-fuzzy-match t
        helm-imenu-fuzzy-match t
        helm-locate-fuzzy-match t
        helm-lisp-fuzzy-completion t
        helm-org-headings-fontify t
        helm-buffer-skip-remote-checking t
        helm-display-header-line nil)

  ;; Key bindings
  (global-set-key (kbd "C-c h") 'helm-command-prefix) ;; Replace default C-x c command because it is too close to exit command (C-x C-c)
  (global-unset-key (kbd "C-x c")) ;; Replace default C-x c command because it is too close to exit command (C-x C-c)

  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ;; Add tab as persistent action key (easier during typing)
  (define-key helm-map (kbd "C-z") 'helm-select-action) ;; Perform select action using C-z (perform actions on selected candidates)

  (define-key helm-grep-mode-map (kbd "<return>")  'helm-grep-mode-jump-other-window)
  (define-key helm-grep-mode-map (kbd "n")  'helm-grep-mode-jump-other-window-forward)
  (define-key helm-grep-mode-map (kbd "p")  'helm-grep-mode-jump-other-window-backward)

  (global-set-key (kbd "M-x") 'helm-M-x)
  (global-set-key (kbd "M-y") 'helm-show-kill-ring)
  (global-set-key (kbd "C-x b") 'helm-buffers-list)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  (global-set-key (kbd "C-c r") 'helm-recentf)
  (global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)
  (global-set-key (kbd "C-c h o") 'helm-occur)

  (define-key 'help-command (kbd "C-f") 'helm-apropos)
  (define-key 'help-command (kbd "r") 'helm-info-emacs)
  (define-key 'help-command (kbd "C-l") 'helm-locate-library)

  ;; Show minibuffer history with Helm
  (define-key minibuffer-local-map (kbd "M-p") 'helm-minibuffer-history)
  (define-key minibuffer-local-map (kbd "M-n") 'helm-minibuffer-history)

  (define-key global-map [remap find-tag] 'helm-etags-select)

  (define-key global-map [remap list-buffers] 'helm-buffers-list)


  (use-package helm-swoop
    :bind (("C-c s" . helm-swoop))
    :config
    ;; When doing isearch, hand the word over to helm-swoop
    (define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)

    ;; From helm-swoop to helm-multi-swoop-all
    (define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop)

    ;; Save buffer when helm-multi-swoop-edit complete
    (setq helm-multi-swoop-edit-save t)

    ;; If this value is t, split window inside the current window
    (setq helm-swoop-split-with-multiple-windows t)

    ;; Split direcion. 'split-window-vertically or 'split-window-horizontally
    (setq helm-swoop-split-direction 'split-window-horizontally)

    ;; If nil, you can slightly boost invoke speed in exchange for text color
    (setq helm-swoop-speed-or-color t))

  (use-package helm-projectile
    :init
    (helm-projectile-on)
    (setq projectile-completion-system 'helm)
    (setq projectile-indexing-method 'alien))
  (helm-mode 1)
  (projectile-global-mode 1))

(provide 'setup-helm)
