(use-package plantuml-mode
  :config
  (setq plantuml-jar-path "/home/tms/Binaries/plantuml/plantuml.jar"))
(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t)))
(setq org-plantuml-jar-path
      (expand-file-name "~/Binaries/plantuml/plantuml.jar"))
(provide 'setup-plantuml)
