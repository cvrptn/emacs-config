(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package auctex
  :defer t
  :config
  (setq TeX-parse-self t)
  (setq TeX-auto-save t)
  (setq TeX-save-query nil)
  (setq TeX-PDF-mode t)
  (add-hook 'LaTeX-mode-hook 'visual-line-mode)
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode))


(provide 'setup-latex)
