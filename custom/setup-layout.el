(menu-bar-mode -1)
(tool-bar-mode -1)


(use-package color-theme-sanityinc-tomorrow
  :config
  (load-theme 'sanityinc-tomorrow-night t))

(use-package nyan-mode
  :config
  (if window-system (nyan-mode)))

(use-package buffer-move
  :bind (("<M-S-up>" . buf-move-up)
         ("<M-S-down>" . buf-move-down)
         ("<M-S-left>" . buf-move-left)
         ("<M-S-right>" . buf-move-right)))

(define-globalized-minor-mode global-purpose-mode
  purpose-mode purpose-mode)

;; (use-package window-purpose
;;   :init
;;   (setq purpose-preferred-prompt 'helm)
;;   :config
;;   (define-key purpose-mode-map (kbd "C-X C-f") nil)
;;   (define-key purpose-mode-map (kbd "C-X b") nil)
;;   (add-to-list 'purpose-user-mode-purposes '(python-mode . py))
;;   (add-to-list 'purpose-user-mode-purposes '(inferior-python-mode . py-repl))
;;   (add-to-list 'purpose-user-name-purposes '(*SPEEDBAR* . speedbar))
;;   (purpose-compile-user-configuration)
;;   (global-purpose-mode))

(defun split-root-window (size direction)
  (split-window (frame-root-window)
                (and size (prefix-numeric-value size))
                direction))

(defun split-root-window-below (&optional size)
  (interactive "P")
  (split-root-window size 'below))

(global-set-key (kbd "C-x 9") 'split-root-window-below)

(provide 'setup-layout)
