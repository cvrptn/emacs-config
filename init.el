(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(add-to-list 'load-path "~/.emacs.d/custom")

(require 'setup-general)
(require 'setup-helm)
(require 'setup-latex)
(require 'setup-layout)
(require 'setup-org)
(require 'setup-python)
(require 'setup-plantuml)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (plantuml-mode zygospore ws-butler volatile-highlights use-package sr-speedbar region-bindings-mode nyan-mode multiple-cursors iedit helm-swoop helm-projectile exec-path-from-shell dtrt-indent company-jedi company-anaconda color-theme-sanityinc-tomorrow clean-aindent-mode buffer-move anzu))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
